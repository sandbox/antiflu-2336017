<?php

/**
 * @file
 * Defines admin configuration page for the Zoho API module
 */

/**
 * Menu callback for admin/config/zoho. Admin settings form.
 */
function zoho_support_admin_settings_form($form, &$form_state) {
  $form = array();

  $form['zoho_support_authtoken'] = array(
    '#type' => 'textfield',
    '#title' => t('Zoho API Authtoken'),
    '#default_value' => variable_get('zoho_support_authtoken', ''),
    '#description' => t('Enter a Valid API Authtoken. To find your token or create a new one, visit Zoho Support.')
  );

  $form['zoho_support_portal'] = array(
      '#type' => 'textfield',
      '#title' => t('Portal name'),
      '#description' => t('The name of the Zoho Support portal to connect to.'),
      '#default_value' => variable_get('zoho_support_portal', 'onsite'),
      '#size' => 30,
  );
  
  $form['zoho_support_access_mode'] = array(
      '#type' => 'select',
      '#title' => t('Zoho Support access mode'),
      '#description' => t('Mode of access. Select TEST_READONLY for read-only testing, TEST_WRITE to write only to the test record and select LIVE to enable writing to any record.'),
      '#options' => array(
	  'TEST_READONLY' => t('TEST_READONLY'),
	  'TEST_WRITE' => t('TEST_WRITE'),
          'LIVE' => t('LIVE'),
      ),
      '#default_value' => variable_get('zoho_support_access_mode', 'TEST_READONLY'),
  );

  $form['zoho_support_test_record'] = array(
      '#type' => 'textfield',
      '#title' => t('Test record'),
      '#description' => t('The Request Id of the record that will be writable when TEST_WRITE mode is enabled.'),
      '#default_value' => variable_get('zoho_support_test_record', '90003'),
      '#size' => 8,
  );

  $form['#validate'][] = 'zoho_support_validate_settings';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings'),
  );
  return $form;
}

/**
 * Validate function for admin settings form.
 */
function zoho_support_validate_settings($form, &$form_state) {
  if (($form_state['values']['zoho_support_authtoken'] == '')) {
    form_set_error('', t('Please fill api Authtoken.'));
  }
  $id = $form_state['values']['zoho_support_test_record'];
  if ((!empty($id) && (!is_numeric($id))))
    form_set_error('', t('Please fill in a numeric Request Id or leave the value empty.'));
}

/**
 * Submit function for admin settings form.
 */
function zoho_support_admin_settings_form_submit($form, &$form_state) {
  if ($form_state['values']['zoho_support_authtoken'] != '') {
    variable_set('zoho_support_authtoken', $form_state['values']['zoho_support_authtoken']);
  }
  variable_set('zoho_support_portal', $form_state['values']['zoho_support_portal']);
  variable_set('zoho_support_access_mode', $form_state['values']['zoho_support_access_mode']);
  variable_set('zoho_support_test_record', $form_state['values']['zoho_support_test_record']);
}

