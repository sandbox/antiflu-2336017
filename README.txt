Installation Instructions:

1. Copy the zoho_support module directory into your modules directory.

2. Enable zoho_support module at: Administer > Modules (admin/modules)

3. Go to Administer > People > Permissions (admin/people/permissions) 
   and grant 'Administer zoho support api module' permission to the needed user roles.

4. Configure Zoho Support account settings at admin/config/services/zoho_support


Author: Tom Blauwendraat <antiflu@gmail.com>
Based on 'zoho' module by Chakrapani Reddivari 

