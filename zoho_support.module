<?php

/**
 * @file
 * The module that defined the callbacks to communicate with zoho crm.
 */

define('ZOHO_SUPPORT_URL', 'https://support.zoho.com/api/xml/');

// In the URL, a request is called 'requests', in the GET parameters 'Request', and in the
// returned XML it is called 'Cases'. This function groups them all under ZOHO_SUPPORT_REQUEST.
define('ZOHO_SUPPORT_REQUEST', 0);
function zoho_support_type($type, $trans) {
  switch($type) {
    case ZOHO_SUPPORT_REQUEST:
      switch($trans) {
        case 0:
          return 'requests';
        case 1:
          return 'Request';
        case 2:
          return 'Cases';
      }
  }
  return 'unknown';
}

/**
 * Implements hook_menu().
 */
function zoho_support_menu() {
  $items = array();
  $items['admin/config/services/zoho_support'] = array(
    'title' => 'Zoho Support integration settings',
    'description' => 'Configure the settings for Zoho Support integration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('zoho_support_admin_settings_form'),
    'file' => 'zoho_support.admin.inc',
    'access arguments' => array('administer zoho support api'),
    'weight' => -10,
  );
  $items['admin/config/services/zoho_support/settings'] = array(
    'title'  => 'Settings',
    'type'   => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  return $items;
}

/**
 * Getting data from zoho support.
 *
 * @param string $search_condition
 *   Search condition based on which records will be fetched.
 *   e.g,'(Email|=|xyz@xyz.com)'
 * @param string $type
 *   Type of records to be fetched. e.g,'Leads'
 * @param string $select_columns
 *   Columns of the records to be fetched.
 *   e.g, 'Leads(First Name,Last Name,Email)'
 *
 * @returns object
 *   Returns an object with following keys:
 *   success will contain TRUE if the request is succesfull else FALSE.
 *   error will contain error code if the request was not successful.
 *   message will contain success message or error message.
 *   records will contain an associated array of records with data
 *   if the request is successful.
 */
function zoho_support_get_records($department, $type = ZOHO_SUPPORT_REQUEST, 
    $searchfield = false, $searchvalue = false, $columns = array()) 
{
  $portal = variable_get('zoho_support_portal');
  $type0 = zoho_support_type($type, 0);
  $type1 = zoho_support_type($type, 1);
  $result = (object) array();
  $token = variable_get('zoho_support_authtoken', FALSE);
  if (!$token) {
    $link = l(t('Zoho Configurations'), 'admin/config/services/zoho');
    watchdog('zoho', 'Zoho Support Authtoken should be configured at admin > config > services > zoho support', 
      array(), WATCHDOG_ERROR, $link);
    $result->success = FALSE;
    $result->message = 'Zoho Support API is not configured.';
    return $result;
  }
  $params = "?authtoken=$token"; 
  $params .= "&portal=" . rawurlencode($portal);
  $params .= "&department=" . rawurlencode($department);
  if ($columns) {
    $select_columns = rawurlencode(join(',', $columns));
    $params .= "&selectfields=$type1($select_columns)";
  } else {
    $params .= "&selectfields=All";
  }
  if ($searchfield) {
    $params .= '&searchfield=' . rawurlencode($searchfield);
    $params .= '&searchvalue=' . rawurlencode($searchvalue);
    $url = ZOHO_SUPPORT_URL . "$type0/getrecordsbysearch" . $params;
  } else {
    $url = ZOHO_SUPPORT_URL . "$type0/getrecords" . $params;
  }
  $response = drupal_http_request($url);
  if ($response->code == 200) {
    $xml = simplexml_load_string($response->data, 'SimpleXMLElement', LIBXML_NOCDATA);
    if (isset($xml->result)) {
      $result->success = TRUE;
      $result->message = (string) $xml->result->message;
      $result->records = zoho_support_parse_records($xml, $type);
      $result->simplexml = $xml;
    }
    elseif (isset($xml->error)) {
      $result->success = FALSE;
      $result->message = (string) $xml->error->message;
      $result->error  = (string) $xml->error->code;
    }
  }
  else {
    $result->success = FALSE;
    $result->error = $response->code;
    $result->message = $response->error;
  }
  return $result;
}

function _find_request_by_id($departments, $ticketid, $recordnames) {
  $datasets = array();
  $result = array('success' => false);
  foreach ($departments as $department) {
    $dataset = zoho_support_get_records($department,
        ZOHO_SUPPORT_REQUEST, 'Request ID', $ticketid, $recordnames);
    if (!$dataset->success && ($dataset->error != 4832)) {
      $result['error'] = "No results: " . $dataset->message . " (" . $dataset->error . ")";
      return $result;
    }
    if ($dataset->success) {
      $datasets[$department] = $dataset;
    }
  }
  if (!count($datasets)) {
    $result['error'] = "Invalid ticket number ($ticketid)";
    return $result;
  }

  // Find first record with this ticket number, in any dataset
  foreach ($datasets as $department => $dataset) {
    foreach ($dataset->records as $record) {
      break;
    }
  }
  // Copy fields to $data, defaulting to empty string
  $data = array();
  foreach ($recordnames as $recordname) {
    if (array_key_exists($recordname, $record))
      $data[$recordname] = $record[$recordname];
    else
      $data[$recordname] = '';
  }
  $result['success'] = true;
  $result['data'] = $data;
  $result['xml'] = $dataset->simplexml;
  $result['department'] = $department;
  return $result;
}

function zoho_support_update_request_fields($departments, $ticketid, $fields) {
  $mode = variable_get('zoho_support_access_mode');
  $test_ticket = variable_get('zoho_support_test_record');
  if (($mode == 'LIVE') || (($mode == 'TEST_WRITE') && ($ticketid == $test_ticket))) {
    $result = _find_request_by_id($departments, $ticketid, array('CASEID'));
    if ($result['success']) {
      $Cases = $result['xml']->result->children()[0];
      $recordid = $Cases->xpath('row[@no="1"]/fl[@val="CASEID"]')[0];
      $newCases = new SimpleXMLElement('<Cases><row no="1"></row></Cases>');
      $elms = $newCases->xpath('row')[0];
      foreach ($fields as $fieldname => $fieldvalue) {
	$elm = $elms->addChild('fl');
	$elm->addAttribute('val', $fieldname);
	$elm[0] = $fieldvalue;
      }
      $xml = (string)$newCases->asXML();
      $response = zoho_support_update_record($result['department'], ZOHO_SUPPORT_REQUEST, $xml, $recordid);
    }
  } else {
    // drupal_set_message('Write action to ZOHO blocked');
  }
}

function zoho_support_update_record($department, $type, $xml, $recordid) 
{
  $portal = variable_get('zoho_support_portal');
  $type0 = zoho_support_type($type, 0);
  $type1 = zoho_support_type($type, 1);
  $result = (object) array();
  $token = variable_get('zoho_support_authtoken', FALSE);
  if (!$token) {
    $link = l(t('Zoho Configurations'), 'admin/config/services/zoho_support');
    watchdog('zoho', 'Zoho Support Authtoken should be configured at admin > config > services > zoho support', 
      array(), WATCHDOG_ERROR, $link);
    $result->success = FALSE;
    $result->message = 'Zoho Support API is not configured.';
    return $result;
  }
  $params = "?authtoken=$token"; 
  $params .= "&portal=" . rawurlencode($portal);
  $params .= "&department=" . rawurlencode($department);
  $params .= "&xml=" . rawurlencode($xml);
  $params .= "&id=" . rawurlencode($recordid);
  $url = ZOHO_SUPPORT_URL . "$type0/updaterecords" . $params;
  $response = drupal_http_request($url);
  if ($response->code == 200) {
    //echo '<pre>' . htmlentities(print_r($response->data, true)) . '</pre>';
    $xml = simplexml_load_string($response->data, 'SimpleXMLElement', LIBXML_NOCDATA);
    if (isset($xml->result)) {
      $result->success = TRUE;
    }
    else {
      $result->success = FALSE;
    }
  }
  else {
    $result->success = FALSE;
    $result->error = $response->code;
    $result->message = $response->error;
  }
  return $result;
}

/**
 * Parse xml object into an array.
 *
 * @param object $xml
 *   An object of type simplexmlelement returned by simplexml_load_string().
 * @param string $type
 *   Type of records being fetched. e.g,'Leads'
 *
 * @returns array
 *   An associated array of records with fiekds and values.
 *   $records[row value][field value]
 */
function zoho_support_parse_records($xml, $typenum) {
  $type = zoho_support_type($typenum, 2);
  $records = array();
  foreach ($xml->result->$type->row as $r => $row) {
    foreach ($row->fl as $fl) {
      $field = (string) $fl['val'];
      $records[$r][$field] = (string) $fl;
    }
  }
  return $records;
}

/**
 * Implements hook_permission().
 */
function zoho_support_permission() {
  return array(
    'administer zoho support api' => array(
      'title' => t('Administer zoho support api module'),
      'description' => t('Perform administration tasks for zoho support api module.'),
    ),
  );
}

